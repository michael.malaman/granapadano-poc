'use strict'

var json = require('./data.json'); 

var jsonParsed = productParse(json)

console.log(jsonParsed)


function productParse(json){

    var all_farmerbatches = {}
    var all_deliveries = {}

    for (var key in json.batchraws) {
        var batchraws = json.batchraws[key];
        //console.log(batchraws)
        for (var key2 in batchraws) {
            var deliveries = batchraws[key2];
            //console.log(deliveries)
            for (var key3 in deliveries) {
                var delivery = deliveries[key3];
                if (typeof delivery.id !== 'undefined') {
                    all_deliveries[delivery.id] = delivery
                }   
                //console.log(delivery)
                for (var key4 in delivery.farmerbatches) {
                    var farmerbatch = delivery.farmerbatches[key4];
                    //console.log(farmerbatch.id)
                    if (typeof farmerbatch.id !== 'undefined') {
                        all_farmerbatches[farmerbatch.id] = farmerbatch
                    }   
                }
            }
        }
    }
    

    return {'deliveries':all_deliveries , 'farmerbatches': all_farmerbatches}
}


