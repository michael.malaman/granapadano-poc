// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { i18n } from '@/plugins/i18n'
import { Trans } from './plugins/Translation'
import 'vuetify/dist/vuetify.min.css'
import Vuetify from 'vuetify'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist/dist/umd'
import ReadMore from 'vue-read-more'
import VueMoment from 'vue-moment'
import VueScrollactive from 'vue-scrollactive'
import VueAwesomeSwiper from 'vue-awesome-swiper'

// require styles
import 'swiper/dist/css/swiper.css'

Vue.use(VueAwesomeSwiper)
Vue.use(ReadMore)
Vue.use(Vuex)
Vue.use(Vuetify)
Vue.use(VueMoment)
Vue.use(VueScrollactive)

Vue.prototype.$i18nRoute = Trans.i18nRoute.bind(Trans)

Vue.config.productionTip = false

const vuexLocalStorage = new VuexPersist({
  key: 'vuex', // The key to store the state on in the storage provider.
  storage: window.localStorage // or window.sessionStorage or localForage
  // Function that passes the state and returns the state with only the objects you want to store.
  // reducer: state => state,
  // Function that passes a mutation and lets you decide if it should update the state in localStorage.
  // filter: mutation => (true)
})
const store = new Vuex.Store({
  state: {
    batchID: null,
    posts: {},
    parsed: {},
    batchraw: null
  },
  getters: {
    BATCHID: state => {
      return state.batchID
    },
    BATCHINFO: state => {
      return state.posts
    },
    BATCHRAW: state => {
      return state.batchID
    },
    PARSEDINFO: state => {
      return state.parsed
    }
  },
  mutations: {
    SET_BATCH: (state, payload) => {
      state.batchID = payload
    },
    SET_BATCHINFO: (state, payload) => {
      state.posts = payload
    },
    SET_BATCHRAW: (state, payload) => {
      state.batchID = payload
    },
    SET_DELIVERY_FARMER: (state, payload) => {
      state.parsed = payload
    }
  },
  actions: {
    async update_batchinfo (context, payload) {
      context.commit('SET_BATCHINFO', payload)
    },
    async update_batch (context, payload) {
      context.commit('SET_BATCH', payload)
    },
    async update_batchraw (context, payload) {
      context.commit('SET_BATCHRAW', payload)
    },
    async update_delivery_farmer (context, payload) {
      context.commit('SET_DELIVERY_FARMER', payload)
    }
  },
  plugins: [vuexLocalStorage.plugin]
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  Vuetify,
  router,
  i18n,
  store,
  render: (h) => h(App)
})
