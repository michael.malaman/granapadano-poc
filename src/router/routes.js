import { Trans } from '@/plugins/Translation'

function load (component) {
  // '@' is aliased to src/components
  return () => import(/* webpackChunkName: "[request]" */ `@/pages/${component}.vue`)
}

export default [
  {
    path: '/:lang',
    component: {
      template: '<router-view></router-view>'
    },
    beforeEnter: Trans.routeMiddleware,
    children: [
      {
        path: '/:lang/',
        name: 'Home',
        component: load('MainContainer')
      },
      {
        path: '/:lang/info',
        name: 'info',
        component: load('Info')
      },
      {
        path: '/:lang/about',
        name: 'about',
        component: load('About')
      },
      {
        path: '/:lang/certificato',
        name: 'certificato',
        component: load('Certificato')
      },
      {
        path: '/:lang/contributore',
        name: 'contributore',
        component: load('Contributore')
      },

      {
        path: '/:lang/analisi',
        name: 'analisi',
        component: load('Analisi')
      },
      {
        path: '/:lang/trasformazione/allevamento',
        name: 'allevamento',
        component: load('trasformazione/allevamento')
      },
      {
        path: '/:lang/trasformazione/informazionisulluogo',
        name: 'informazionisulluogo',
        component: load('trasformazione/informazionisulluogo')
      },
      {
        path: '/:lang/trasformazione/informazionisostenibilita',
        name: 'informazionisostenibilita',
        component: load('trasformazione/informazionisostenibilita')
      },
      {
        path: '/:lang/trasformazione/conferimento',
        name: 'conferimento',
        component: load('trasformazione/conferimento')
      },
      {
        path: '/:lang/trasformazione/analisiprodottofinito',
        name: 'trasformazione_analisiprodottofinito',
        component: load('trasformazione/analisiprodottofinito')
      },
      {
        path: '/:lang/trasformazione/trasporto',
        name: 'trasporto',
        component: load('trasformazione/trasporto')
      },
      {
        path: '/:lang/trasformazione/stoccaggio',
        name: 'stoccaggio',
        component: load('trasformazione/stoccaggio')
      },
      {
        path: '/:lang/trasformazione/filatura',
        name: 'trasformazione_filatura',
        component: load('trasformazione/filatura')
      },
      {
        path: '/:lang/trasformazione/formaturaerassodamento',
        name: 'trasformazione_formaturaerassodamento',
        component: load('trasformazione/formaturaerassodamento')
      },
      {
        path: '/:lang/trasformazione/maturazione',
        name: 'trasformazione_maturazione',
        component: load('trasformazione/maturazione')
      },
      {
        path: '/:lang/trasformazione/cagliatura',
        name: 'trasformazione_cagliatura',
        component: load('trasformazione/cagliatura')
      },
      {
        path: '/:lang/mappa',
        name: 'mappa',
        component: load('Mappa')
      },
      {
        path: '/:lang/Info',
        name: 'Info',
        component: load('Info')
      },
      {
        path: '/:lang/DettagliMungitura',
        name: 'DettagliMungitura',
        component: load('DettagliMungitura')
      },
      {
        path: '/:lang/NotFound',
        name: 'notfound',
        component: load('ProductNotFound')
      },
      {
        path: '*',
        name: '404',
        component: load('404')
      }
    ]
  },
  {
    // Redirect user to supported lang version.
    path: '*',
    redirect (to) {
      return Trans.getUserSupportedLang()
    }
  }
]
