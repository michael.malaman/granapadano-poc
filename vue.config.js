// vue.config.js file to be place in the root of your repository
// make sure you update `yourProjectName` with the name of your GitLab project

module.exports = {
  transpileDependencies: ['vuex-persist'],
  publicPath: process.env.NODE_ENV === 'production'
    ? '/'
    : '',
  outputDir:'public'
  }